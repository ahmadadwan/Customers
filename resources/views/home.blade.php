@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">

                      <tr>
                        <th> Email </th>
                        <td> {{\Auth::user()->email}} </td>
                      </tr>
                      <tr>
                        <th> Phone </th>
                        <td> {{\Auth::user()->phone}} </td>
                      </tr>
                      <tr>
                        <th> Full Name </th>
                        <td> {{\Auth::user()->name}} </td>
                      </tr>
                      <tr>
                        <th> Profile Picture </th>
                        <td> <img src="{{asset('thumbnail_images')}}/<?=\Auth::user()->photo ?>"> </img> </td>
                      </tr>




                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
