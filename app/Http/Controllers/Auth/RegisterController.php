<?php


namespace App\Http\Controllers\Auth;


use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Illuminate\Http\Request;
use Image;



class RegisterController extends Controller
{

    use RegistersUsers;
    use VerifiesUsers;


    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest',['except' => ['getVerification', 'getVerificationError']]);
    }
    protected function validator(array $data)
        {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'photo'=>'required'
            ]);
        }


        /**
         * Create a new user instance after a valid registration.
         *
         * @param  array  $data
         * @return User
         */
        protected function create(array $data)
        {
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'photo'=>$data['photo']

            ]);
        }


    public function register(Request $request)
    {

        $photo = $request->file('photo');
        $imagename = time().'.'.$photo->getClientOriginalExtension();
        $destinationPath = public_path('/thumbnail_images');
        $thumb_img = Image::make($photo->getRealPath())->resize(150, 150);
        $thumb_img->save($destinationPath.'/'.$imagename,80);
        $user = new User();
        $user->name = $request->input('name');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->photo = $imagename ;
        $user->remember_token = $request->input('remember_token'); ;
        $user->save();
        UserVerification::generate($user);
        UserVerification::send($user, 'My Custom E-mail Subject');
        return back()->withAlert('Register successfully, please verify your email.');

    }
}
